package com.devcamp.j04_javabasic.s10;

public class CPet extends CAnimal {
    protected int age;
    protected String name;

    // @Override
    public  void animalSound() {
        System.out.println("Pet sound...");
    }
    
   // @Override
    public void eat() {
        System.out.println("Pet eating...");
    }

    public void print() {
    }

    public void play() {
    }
    public static void main(String[] args) throws Exception {
        CPet myPet = new CPet();
        myPet.name = "Kiya";
        myPet.age = 2;
        myPet.animalClass = AnimalClass.mamals;
        myPet.eat();
        myPet.animalSound();
        myPet.play();
        myPet.print();
    }
}
