package com.devcamp.j04_javabasic.s10;

public abstract class CAnimal {
    public AnimalClass animalClass;
    abstract public void animalSound();
    public  void eat() {
        System.out.println("Animal is eating...");
    }
}
