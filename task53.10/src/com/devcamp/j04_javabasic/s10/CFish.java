package com.devcamp.j04_javabasic.s10;

public class CFish extends CPet implements ISwimable {
    @Override
    public void swim() {
        System.out.println("fish is swimming...");
    }

    public static void main(String[] args) {
        CPet myFish = new CFish();
        myFish.age = 2;
        myFish.name = "My Fish";
        myFish.animalClass = AnimalClass.fish;
        myFish.eat();
        myFish.animalSound();
        myFish.play();
        myFish.print();
        ((CFish)myFish).swim();
    }
}
